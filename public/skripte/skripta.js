/* global L, $ */

// seznam z markerji na mapi
var markerji = [];

var mapa;

const FRI_LAT = 46.05004;
const FRI_LNG = 14.46931;

/**
 * Ko se stran naloži, se izvedejo ukazi spodnje funkcije
 */
window.addEventListener('load', function () {

  // Osnovne lastnosti mape
  var mapOptions = {
    center: [FRI_LAT, FRI_LNG],
    zoom: 9,
    maxZoom: 12
  };

  // Ustvarimo objekt mapa
  mapa = new L.map('mapa_id', mapOptions);

  // Ustvarimo prikazni sloj mape
  var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

  // Prikazni sloj dodamo na mapo
  mapa.addLayer(layer);

  // Akcija ob kliku na gumb za prikaz zgodovine
  $("#prikaziZgodovino").click(function() {
    window.location.href = "/zgodovina";
  });

  function obKlikuNaMapo(e) {
    // Izbriši prejšnje oznake
    for (var i = 0; i < markerji.length; i++) {
      mapa.removeLayer(markerji[i]);
    }

    var koordinate = e.latlng;
    
    $.getJSON('https://api.lavbic.net/kraji/lokacija?lat='+koordinate.lat+ '&lng=' + koordinate.lng, function(data) {
        // Prikaži seznam 5 najbližjih krajev
        $("#bliznjiKraji").html("<p><h3>Bližnji kraji</h3><br><b>"+ data[0].postnaStevilka + "</b> "+data[0].kraj+"<br><b>"+ data[1].postnaStevilka + "</b> "+data[1].kraj+"<br><b>"+ data[2].postnaStevilka + "</b> "+data[2].kraj+
        "<br><b>"+ data[3].postnaStevilka + "</b> "+data[3].kraj+"<br><b>"+ data[4].postnaStevilka + "</b> "+data[4].kraj+"</p>");
        
        $.get("/vreme/"+data[0].postnaStevilka, function(podatki) {
          // Prikaži vremenske podatke najbližjega kraja
          $("#vremeOkolica").html("<p>Vreme v okolici kraja <br><h3>"+ data[0].kraj +"</h3><br><img src=/slike/"+ podatki.ikona+"> "+ podatki.opis+" "+podatki.temperatura +"<br></p>");
        });
        
        
        // Shrani podatke najbližjega kraja
        $.get('/zabelezi-zadnji-kraj/' + JSON.stringify({
          postnaStevilka: data[0].postnaStevilka,
          kraj: data[0].kraj,
          vreme: "neznano",
          temperatura: 10
        }), function(t) {
          $("#prikaziZgodovino").html("Prikaži zgodovino "+ 5  + " krajev");
        });
    });
    

    // Dodaj trenutno oznako
    dodajMarker(koordinate.lat, koordinate.lng,
      "<b>Izbrana lokacija</b><br>(" + koordinate.lat + ", " + koordinate.lng + ")");
    
  }

  mapa.on('click', obKlikuNaMapo);

  // Na začetku postavi lokacijo na FRI
  var FRIpoint = new L.LatLng(FRI_LAT, FRI_LNG);
  mapa.fireEvent('click', {
    latlng: FRIpoint,
    layerPoint: mapa.latLngToLayerPoint(FRIpoint),
    containerPoint: mapa.latLngToContainerPoint(FRIpoint)
  });
});


/**
 * Dodaj izbrano oznako na zemljevid na določenih GPS koordinatah
 *
 * @param lat zemljepisna širina
 * @param lng zemljepisna dolžina
 * @param opis sporočilo, ki se prikaže v oblačku
 */
function dodajMarker(lat, lng, opis) {
  var ikona = new L.Icon({
    iconUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' +
      'marker-icon-2x-blue.png',
    shadowUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' +
      'marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
  });

  var marker = L.marker([lat, lng], {icon: ikona});
  marker.bindPopup("<div>" + opis + "</div>").openPopup();
  marker.addTo(mapa);
  markerji.push(marker);
}
